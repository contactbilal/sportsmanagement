/* FORM WIZARD VALIDATION SIGN UP ======================================== */

// $('form#custom').attr('action', 'quotation-wizard-send.php');

$(function() {
    'use strict';
    $('#custom').stepy({
        backLabel: 'Previous step',
        block: true,
        errorImage: true,
        nextLabel: 'Next step',
        titleClick: true,
        description: true,
        legend: false,
        validate: true
    });


    $('#custom').validate({

        errorPlacement: function(error, element) {

            error.insertAfter(element);
        },
        rules: {
            'payment': 'required',
            'user_resume': 'required',
            'full_name': 'required',
            'book_seats': 'required',
            'address': 'required',
            'mobile': 'required',
            'budget': 'required',
            'firstname_quote': 'required',
            'lastname_quote': 'required',
            'email': 'required',
            'phone_quote': 'required',
            'message_general': 'required',
            'terms': 'required' // BE CAREFUL: last has no comma
        },
        messages: {
            'payment': { required: 'Payment mode is required' },
            'first_name': { required: 'Add your first name please' },
            'book_seats': { required: 'Add your last name please' },
            'address': { required: 'Add your shipping address' },
            'mobile': { required: 'Mobile number is required' },
            'budget': { required: 'Budget required' },
            'firstname_quote': { required: 'First name required' },
            'lastname_quote': { required: 'Last name required' },
            'email': { required: 'Email required' },
            'phone_quote': { required: 'Phone required' },
            'message_general': { required: 'Description required' },
            'terms': { required: 'Please accept terms' },
        },
        submitHandler: function(form) {
            if ($('input#website').val().length == 0) {
                form.submit();
            }
        }
    });

});