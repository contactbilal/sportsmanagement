@extends("layouts.admin.site")
@section("content")
<h4 class="font-weight-bold py-3 mb-4">
    Add Event <span class="text-muted"></span>
</h4>

<div class="nav-tabs-top">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#user-edit-account">Account</a>
        </li>

    </ul>
    <div class="tab-content">
        <div class="tab-pane fade show active" id="user-edit-account">
            <form action="{{ route('events.store') }}" method="POST" enctype="multipart/form-data">

                @csrf

                <div class="card-body">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {{-- <div class="media align-items-center">
                        <img src="{{ asset('assets/images/client-01.png') }}" id="change-avatar" alt="" class="d-block ui-w-80">
                        <div class="media-body ml-3">
                            <label class="form-label d-block mb-2">Avatar</label>
                            <label class="btn btn-outline-primary btn-sm">
                                Add
                                <input type="file" name="avtar"   onchange="changeAvatar(this);" class="user-edit-fileinput d-none">
                                </label>&nbsp;
                        </div>
                    </div> --}}

                </div>
                <hr class="border-light m-0">
                <div class="card-body pb-2">

                    <div class="form-group">
                        <label class="form-label">Event Name</label>
                        <input type="text" name="event_name" class="form-control mb-1" placeholder="Event Name">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Event Category</label>
                        <select name="category_id" class="custom-select">
                          <option selected disabled hidden>Select Category</option>
                          @isset($categories)
                              @foreach ($categories as $category)
                                <option  value="{{ $category->id ?? '' }}">{{ $category->category_name ?? '' }}</option>
                              @endforeach
                          @endisset
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Location</label>
                        <input type="text" name="location" class="form-control mb-1" placeholder="Location">
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-label">Date</label>
                            <input type="date" name="date" class="form-control" placeholder="Date">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-label">Time</label>
                            <input type="time" name="time" class="form-control" placeholder="Date">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="form-label">Description</label>
                        <textarea name="description" id="" class="form-control" cols="30" rows="10"></textarea>
                    </div>
                </div>
                <div class="text-right mt-3">
                    <button type="submit" class="btn btn-primary">Add </button>&nbsp;
                </div>
            </form>


        </div>
    </div>
</div>
<script>
    function changeAvatar(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
              $('#change-avatar')
                  .attr('src', e.target.result)
                  .width(80)
                  .height(80);
          };
          reader.readAsDataURL(input.files[0]);
      }
  }
</script>
@endsection
