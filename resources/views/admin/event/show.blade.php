@extends("layouts.admin.site")
@section("content")
<div class="media align-items-center py-3 mb-3">
    <img src="assets/img/avatars/5-small.png" alt="" class="d-block ui-w-100 rounded-circle">
    <div class="media-body ml-4">
        {{-- <h4 class="font-weight-bold mb-0">Nelle Maxwell <span class="text-muted font-weight-normal">@nmaxwell</span></h4> --}}
        {{-- <div class="text-muted mb-2">ID: 3425433</div> --}}
        <a href="javascript:void(0)" class="btn btn-primary btn-sm">Edit</a>&nbsp;
        <a href="javascript:void(0)" class="btn btn-default btn-sm">Profile</a>&nbsp;
        <a href="javascript:void(0)" class="btn btn-default btn-sm icon-btn"><i class="ion ion-md-mail"></i></a>
    </div>
</div>


<div class="card">

    <div class="card-body">

        <table class="table user-view-table m-0">
            <tbody>

                <tr>
                    <td>Event Name:</td>
                    <td>{{ $event->event_name }}</td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>{{ $event->date }}</td>
                </tr>
                <tr>
                    <td>Location:</td>
                    <td>{{ $event->location }}</td>
                </tr>

                <tr>
                    <td>About:</td>
                    <td>{{ $event->description }}</td>
                </tr>
            </tbody>
        </table>

    </div>
</div>
@endsection
