@extends('layouts.admin.site')
@section('content')
<h4 class="font-weight-bold py-3 mb-4">
    <span class="text-muted font-weight-light">event </span>
</h4>
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('msg') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
 </div>
<!-- DataTable within card -->

<div class="card">
    <h6 class="card-header">
        <h6 class="card-header text-right">
            <a href="{{ route('events.create') }}" class="btn btn-success" > Add New</a>
        </h6>

    </h6>
    <div class="card-datatable table-responsive">
        <table class="datatables-demo table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Date Time</th>
                    <th>Location</th>
                    <th>Confirm</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

                @isset($events)
                    @if (count($events) > 0)
                        @foreach ($events as $event)
                        <tr class="odd gradeX">
                            <td>{{ $event->id ?? '' }}</td>
                            <td>{{ $event->event_name ?? '' }}</td>
                            <td>{{ $event->category->category_name ?? '' }}</td>
                            <td>{{ $event->date ?? '' }} {{ $event->time ?? '' }}</td>
                            <td>{{ $event->location ?? '' }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-success dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">Confirmed</button>
                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: top, left; top: 25px; left: 0px;">
                                        <a class="dropdown-item">unconfirmed</a>
                                    </div>
                                </div>
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-dark dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">Action</button>
                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: top, left; top: 35px; left: 0px;">
                                        <a href="{{ route('events.show',$event->id) }}" class="dropdown-item"><i class="fa fa-eye"></i> View Event Detail</a>

                                        <a href="{{ route('event.show.team', $event->id) }}" class="dropdown-item"><i class="fa fa-plus"></i> Add Team </a>

                                        <a href="{{ route('events.edit', $event->id) }}" class="dropdown-item"><i class="fa fa-edit"></i> Edit </a>
                                        <form action="{{ route('events.destroy',$event->id) }}" class="d-flex" method="post">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE"> <button class="dropdown-item">
                                    <i class="fa fa-trash"></i> Delete
                                  </button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <div class="m-3 text-center col-12">
                            <p>No Players found...!</p>
                        </div>
                    @endif
                @endisset


            </tbody>
        </table>
    </div>
</div>
@endsection
