@extends('layouts.admin.site')
@section('content')
<h4 class="font-weight-bold py-3 mb-4">
    <span class="text-muted font-weight-light">Teams </span>
</h4>
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('msg') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
 </div>
<!-- DataTable within card -->

<div class="card">

        <h6 class="card-header text-right">
            <a href="{{ route('teams.create') }}" class="btn btn-success" > Add New</a>
        </h6>

    <div class="card-datatable table-responsive">
        <table class="datatables-demo table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Team Name</th>
                    <th>Email</th>
                    <th>Player</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
              @isset($teams)
                  @if (count($teams)>0)
                        @foreach ($teams as $team)
                            <tr class="odd gradeX">
                                <td>{{ $team->id }}</td>
                                <td>{{ $team->team_name }}</td>
                                <td>{{ $team->email }}</td>
                                <td class="text-center">
                                    <a href="{{ route('team.show.player',$team->id) }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Add Player </a>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-dark dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">Action</button>
                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: top, left; top: 35px; left: 0px;">
                                            <a href="viewTeam.html" class="dropdown-item"><i class="fa fa-eye"></i> View  Profile</a>
                                            <a href="{{route('teams.edit',$team->id)}}" class="dropdown-item"><i class="fa fa-edit"></i> Edit </a>
                                            <form action="{{ route('teams.destroy',$team->id) }}" class="d-flex" method="post">
                                                @csrf
                                                <input type="hidden" name="_method" value="DELETE"> <button type="submit" class="dropdown-item">
                                        <i class="fa fa-trash"></i> Delete
                                      </button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                  @else
                  <div class="m-3 text-center col-12">
                    <p>No Players found...!</p>
                 </div>
                  @endif
              @endisset


            </tbody>
        </table>
    </div>
</div>
@endsection
