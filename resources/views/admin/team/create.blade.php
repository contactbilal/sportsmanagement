@extends("layouts.admin.site")
@section("content")
<h4 class="font-weight-bold py-3 mb-4">
    Add Player <span class="text-muted"></span>
</h4>

<div class="nav-tabs-top">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#user-edit-account">Account</a>
        </li>

    </ul>
    <div class="tab-content">
        <div class="tab-pane fade show active" id="user-edit-account">
            <form action="{{ route('teams.store') }}" method="Post" enctype="multipart/form-data" >
                @csrf
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="media align-items-center">
                        <img src="{{ asset('assets/images/client-01.png') }}" id="change-avatar" alt="" class="d-block ui-w-80">
                        <div class="media-body ml-3">
                            <label class="form-label d-block mb-2">Avatar</label>
                            <label class="btn btn-outline-primary btn-sm">
      Add
      <input type="file"  onchange="changeAvatar(this);" name="avtar" class="user-edit-fileinput d-none">
    </label>&nbsp;

                        </div>
                    </div>

                </div>
                <hr class="border-light m-0">
                <div class="card-body pb-2">
                    <div class="form-group">
                        <label class="form-label"> Name</label>
                        <input type="text" name="team_name" class="form-control mb-1" placeholder="Team Name">
                    </div>
                    <div class="form-group">
                        <label class="form-label"> Email</label>
                        <input type="email" name="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Phone</label>
                        <input type="tel"  name="phone" class="form-control" placeholder="Phone">
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">Country</label>
                                <input type="text" name="country" class="form-control" value="Country">
                            </div>

                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">City</label>
                                <input type="text" name="city" class="form-control" placeholder="City">
                            </div>

                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">State</label>
                                <input type="text" name="state" class="form-control" value="State">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right mt-3">
                    <button type="submit" class="btn btn-primary">Save changes</button>&nbsp;
                    <button type="button" class="btn btn-default">Cancel</button>
                </div>
            </form>


        </div>
    </div>
</div>
<script>
     function changeAvatar(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();
           reader.onload = function (e) {
               $('#change-avatar')
                   .attr('src', e.target.result)
                   .width(80)
                   .height(80);
           };
           reader.readAsDataURL(input.files[0]);
       }
   }
</script>
@endsection
