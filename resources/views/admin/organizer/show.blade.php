@extends("layouts.admin.site")
@section("content")

<div class="media align-items-center py-3 mb-3">
    <img src="assets/img/avatars/5-small.png" alt="" class="d-block ui-w-100 rounded-circle">
    <div class="media-body ml-4">
        <h4 class="font-weight-bold mb-0">{{ $organizer->user_name }}<span class="text-muted font-weight-normal">@nmaxwell</span></h4>
        <div class="text-muted mb-2">ID: 3425433</div>
        <a href="{{ route('organizers.edit',$organizer->id) }}" class="btn btn-primary btn-sm">Edit</a>&nbsp;
        <a href="javascript:void(0)" class="btn btn-default btn-sm">Profile</a>&nbsp;
        <a href="javascript:void(0)" class="btn btn-default btn-sm icon-btn"><i class="ion ion-md-mail"></i></a>
    </div>
</div>


<div class="card">

    <div class="card-body">
        @isset($organizer)
        <table class="table user-view-table m-0">
            <tbody>
                <tr>
                    <td>Username:</td>
                    <td>{{ $organizer->user_name }}</td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td>{{ $organizer->name }}</td>
                </tr>
                <tr>
                    <td>E-mail:</td>
                    <td>{{ $organizer->email }}</td>
                </tr>
                <tr>
                    <td>Phone:</td>
                    <td>{{ $organizer->phone }}</td>
                </tr>
                <tr>
                    <td>Status:</td>
                    <td><span class="badge badge-outline-success">Active</span></td>
                </tr>
                <tr>
                    <td>Country:</td>
                    <td>{{ $organizer->country }}</td>
                </tr>
                <tr>
                    <td>City:</td>
                    <td>{{ $organizer->city }}</td>
                </tr>
                <tr>
                    <td>State:</td>
                    <td>{{ $organizer->state }}</td>
                </tr>
            </tbody>
        </table>
        @endisset
    </div>
</div>
@endsection
