@extends('layouts.admin.site')
@section('content')
<h4 class="font-weight-bold py-3 mb-4">
    <span class="text-muted font-weight-light">Category </span>
</h4>
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('msg') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
 </div>
<!-- DataTable within card -->

<div class="card">

        <h6 class="card-header text-right">
            <a href="{{ route('category.create') }}" class="btn btn-success" > Add New</a>
        </h6>

    <div class="card-datatable table-responsive">
        <table class="datatables-demo table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Category Nmae</th>
                    <th>Slug</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
              @isset($categories)
                  @if (count($categories)>0)
                        @foreach ($categories as $category)
                            <tr class="odd gradeX">
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->category_name }}</td>

                                <td class="text-center">
                                    {{ $category->slug }}
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-dark dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">Action</button>
                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: top, left; top: 35px; left: 0px;">
                                            <a href="viewTeam.html" class="dropdown-item"><i class="fa fa-eye"></i> View  Profile</a>
                                            <a href="{{route('category.edit',$category->id)}} " class="dropdown-item"><i class="fa fa-edit"></i> Edit </a>
                                            <form action="{{route('category.destroy',$category->id)}}" class="d-flex" method="post">
                                                @csrf
                                                <input type="hidden" name="_method" value="DELETE"> <button type="submit" class="dropdown-item">
                                        <i class="fa fa-trash"></i> Delete
                                      </button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                  @else
                  <div class="m-3 text-center col-12">
                    <p>No Category found...!</p>
                 </div>
                  @endif
              @endisset


            </tbody>
        </table>
    </div>
</div>
@endsection
