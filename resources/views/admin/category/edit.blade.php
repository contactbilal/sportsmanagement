@extends("layouts.admin.site")
@section("content")
<h4 class="font-weight-bold py-3 mb-4">
    Edit Event <span class="text-muted"></span>
</h4>

<div class="nav-tabs-top">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#user-edit-account">Account</a>
        </li>

    </ul>
    <div class="tab-content">
        <div class="tab-pane fade show active" id="user-edit-account">
            <form action="{{ route( 'category.update',$category->id) }}" method="Post" enctype="multipart/form-data" >
                    @csrf
                    @method('put')

                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                </div>
                <hr class="border-light m-0">
                <div class="card-body pb-2">

                    <div class="card-body pb-2">
                        <div class="form-group">
                            <label class="form-label">Category Name</label>
                            <input type="text" name="category_name" class="form-control mb-1" value="{{ $category->category_name }}" placeholder="Category Name">
                        </div>
                    </div>
                </div>
                <div class="text-right mt-3">
                    <button type="submit" class="btn btn-primary">Save changes</button>&nbsp;
                    <button type="button" class="btn btn-default">Cancel</button>
                </div>
            </form>

        </div>
    </div>
</div>
<script>
    function changeAvatar(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
              $('#change-avatar')
                  .attr('src', e.target.result)
                  .width(80)
                  .height(80);
          };
          reader.readAsDataURL(input.files[0]);
      }
  }
</script>
@endsection
