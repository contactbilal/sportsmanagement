  <!-- Header -->
  <header class="">
    <nav class="navbar navbar-expand-lg">
        <div class="container-fluid header-container">
            <a class="navbar-brand" href="index.html">
                <h2 class="text-dark">SMS-Expo</h2>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('/home') }}">Home
              <span class="sr-only">(current)</span>
            </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/eventList') }}">All Events</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/schedule') }}">Schedule</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/news') }}">News</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/about') }}">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/contact') }}">Contact Us</a>
                    </li>

                    <li class="nav-item">
                        @auth
                            <span class="nav-link"  class="btn btn-success">
                                <a href="{{ route('userdashboard.home') }}">Dashboard</a>
                            /  <a href="#" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                          document.getElementById('logout-form-web').submit();">Logout</a>
                        </span>


             <form id="logout-form-web" action="{{ route('logout') }}" method="POST" class="d-none">
                 @csrf
             </form>

                        @else
                        <span class="nav-link">
                            <a href="{{ url('/signup') }}">Sign Up</a>  /
                            <a href="{{ url('/login') }}">Login</a> </span>
                        @endauth
                    </li>


                </ul>
            </div>
        </div>
    </nav>
</header>
