                <!-- Brand demo (see assets/css/demo/demo.css) -->
                <div class="app-brand demo">
                    <a href="{{ route('home') }}" class="app-brand-text demo sidenav-text font-weight-normal ml-2">SMS</a>
                    <a href="javascript:void(0)" class="layout-sidenav-toggle sidenav-link text-large ml-auto">
                        <i class="ion ion-md-menu align-middle"></i>
                    </a>
                </div>

                <div class="sidenav-divider mt-0"></div>

                <!-- Links -->
                <ul class="sidenav-inner py-1">

                    <li class="sidenav-item">
                        <a href="{{ route('organizers.index') }}" class="sidenav-link"><i class="sidenav-icon ion ion-md-switch"></i>
        <div>Organizer</div>
    </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{ route('data-entry.index') }}" class="sidenav-link"><i class="sidenav-icon ion ion-md-switch"></i>
                          <div>Data Entry Operator</div>
                        </a>
                    </li>
                    <!-- User Management -->
                    <li class="sidenav-item ">
                        <a href="{{ route('users.index') }}" class="sidenav-link  "><i class="sidenav-icon ion ion-md-switch"></i>
        <div>User Management</div>
    </a>
                    </li>



                    <!-- Event Management -->
                    <li class="sidenav-item">
                        <a href="" class="sidenav-link sidenav-toggle"><i class="sidenav-icon ion ion-md-cube"></i>
<div>Event Management</div>
</a>

                        <ul class="sidenav-menu">
                            <li class="sidenav-item">
                                <a href="{{ route('events.index') }}" class="sidenav-link">
                                    <div>All Events</div>
                                </a>
                            </li>
                            <li class="sidenav-item">
                                <a href="{{ route('events.create') }}" class="sidenav-link">
                                    <div>Create Event</div>
                                </a>
                            </li>
                            <li class="sidenav-item">
                                <a href="viewBooking.html" class="sidenav-link">
                                    <div>Confirm Bookings</div>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <!-- Ticket Management -->
                    <li class="sidenav-item">
                        <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i class="sidenav-icon ion ion-md-switch"></i>
      <div>Ticket Management</div>
    </a>
                        <ul class="sidenav-menu">
                            <li class="sidenav-item">
                                <a href="ticketManagement.html" class="sidenav-link">
                                    <div>View Tickets</div>
                                </a>
                            </li>
                            <li class="sidenav-item">
                                <a href="viewTicket.html" class="sidenav-link">
                                    <div>Confirm Tickets</div>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <!--  Team Management -->
                    <li class="sidenav-item">
                        <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i class="sidenav-icon ion ion-md-grid"></i>
<div>Team Management</div>
</a>

                        <ul class="sidenav-menu">
                            <li class="sidenav-item">
                                <a href="{{ route('teams.index') }}" class="sidenav-link">
                                    <div>View Teams</div>
                                </a>
                            </li>

                            {{-- <li class="sidenav-item">
                                <a href="{{ route('team.show.player',$team->id) }}" class="sidenav-link">
                                    <div>select Player</div>
                                </a>
                            </li> --}}

                        </ul>
                    </li>
                    <!-- player Management -->
                    <li class="sidenav-item ">
                        <a href="{{ route('palyers.index') }}" class="sidenav-link  "><i class="sidenav-icon ion ion-md-switch"></i>
        <div>Player Management</div>
    </a>
                    </li>
                    <li class="sidenav-item ">
                        <a href="{{ route('category.index') }}" class="sidenav-link  "><i class="sidenav-icon ion ion-md-switch"></i>
        <div>Categories</div>
    </a>
                    </li>
                    <!-- competition Management -->
                    <li class="sidenav-item ">
                        <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i class="sidenav-icon ion ion-md-pie"></i>
<div>Competition Management</div>
</a>

                        <ul class="sidenav-menu">
                            <li class="sidenav-item">
                                <a href="competitionManagement.html" class="sidenav-link">
                                    <div>Veiw Competition</div>
                                </a>
                            </li>
                            <li class="sidenav-item">
                                <a href="createMatch.html" class="sidenav-link">
                                    <div>Add Competition</div>
                                </a>
                            </li>
                            <li class="sidenav-item">
                                <a href="winnerList.html" class="sidenav-link">
                                    <div>Competition results</div>
                                </a>
                            </li>
                            <li class="sidenav-item">
                                <a href="winnerList.html" class="sidenav-link">
                                    <div>Event Winner List</div>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <!--  News -->
                    <li class="sidenav-item">
                        <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i class="sidenav-icon ion ion-ios-heart"></i>
<div>News</div>
</a>

                        <ul class="sidenav-menu">
                            <li class="sidenav-item">
                                <a href="{{ route('news.index') }}" class="sidenav-link">
                                    <div> News lIST</div>
                                </a>
                            </li>
                            <li class="sidenav-item">
                                <a href="{{ route('news.create') }}" class="sidenav-link">
                                    <div>Add News</div>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <!--  News -->
                    <li class="sidenav-item">
                        <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i class="sidenav-icon ion ion-ios-heart"></i>
<div>Genrate Report</div>
</a>

                        <ul class="sidenav-menu">
                            <li class="sidenav-item">
                                <a href="userReport.html" class="sidenav-link">
                                    <div>User Reports</div>
                                </a>
                            </li>
                            <li class="sidenav-item">
                                <a href="eventReport.html" class="sidenav-link">
                                    <div>Event Reports</div>
                                </a>
                            </li>
                            <li class="sidenav-item">
                                <a href="ticketReport.html" class="sidenav-link">
                                    <div>Tickets Report</div>
                                </a>
                            </li>
                            <li class="sidenav-item">
                                <a href="teamReport.html" class="sidenav-link">
                                    <div>Team Report</div>
                                </a>
                            </li>
                            <li class="sidenav-item">
                                <a href="competionReport.html" class="sidenav-link">
                                    <div>Competition Report</div>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
