@extends('layouts.admin.site')

@section('content')
<h4 class="font-weight-bold py-3 mb-4">
    Welcome / <small> {{Auth::user()->name ?? ''}} </small>
    <div class="text-muted text-tiny mt-1"><small class="font-weight-normal">Today is Tuesday, 8 February 2018</small></div>
</h4>

<!-- Counters -->
<div class="row">
    <div class="col-sm-6 col-xl-3">

        <div class="card mb-4">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="lnr lnr-cart display-4 text-success"></div>
                    <div class="ml-3">
                        <div class="text-muted small">Event Tickets</div>
                        <div class="text-large">2362</div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-sm-6 col-xl-3">

        <div class="card mb-4">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="lnr lnr-users display-4 text-warning"></div>
                    <div class="ml-3">
                        <div class="text-muted small">Users</div>
                        <div class="text-large">
                             {{ $totalUsers ?? '-' }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-sm-6 col-xl-3">

        <div class="card mb-4">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="lnr lnr-earth display-4 text-info"></div>
                    <div class="ml-3">
                        <div class="text-muted small">New Event</div>
                        <div class="text-large"> {{ $totalEvent ?? '-' }}</div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-sm-6 col-xl-3">

        <div class="card mb-4">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="lnr lnr-earth display-4 text-info"></div>
                    <div class="ml-3">
                        <div class="text-muted small">Done Event</div>
                        <div class="text-large">0</div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- / Counters -->



<div class="row">
    <div class="col-md-6 col-lg-12 col-xl-6">
        <!-- Comments -->
        <div class="card mb-4">
            <h6 class="card-header">Latest Events</h6>
            <div class="card-body">
                <div class="media pb-1 mb-3">
                    <img src="assets/img/avatars/9-small.png" class="d-block ui-w-40 rounded-circle" alt>
                    <div class="media-body ml-3">
                        <h4 class="font-weight-bold">
                        {{ $latestEvent->event_name ?? '-' }}
                            <div class="text-muted text-tiny mt-1"><small class="font-weight-normal">{{ $latestEvent->category_name ?? '' }}</small></div>
                        </h4>
                        @if(isset($latestEvent->id))
                                    <span class="text-muted">Organized By</span>
                                    <a href="javascript:void(0)">{{ Auth::user()->name ?? '-'}}</a>

                        @else
                            <h1 class="text-center">
                                No event Found
                            </h1>
                        @endif


                        <p class="my-1">
                            {{ $latestEvent->description ?? '-'  }}
                        </p>
                        <div class="clearfix">
                            @if(isset($latestEvent->id))
                                <a href="javascript:void(0)" class="float-right text-lightest small">
                                    <button type="button" class="btn btn-sm btn-info">Confirm Event</button>
                                </a>
                            @endif

                            <span class="float-left text-muted small">
                                @if(isset($latestEvent->created_at))
                                        <span>  {{ $latestEvent->created_at->format('d-m-y') }}</span>

                                @else
                                         <span>-</span>
                                @endif
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <a href="{{ route('events.index') }}" class="card-footer d-block text-center text-body small font-weight-semibold">See All Latest Event</a>
        </div>
        <!-- / Comments -->

    </div>
    <div class="col-md-6 col-lg-12 col-xl-6">

        <!-- Support tickets -->
        <div class="card mb-4 text-center">
            <h6 class="card-header">Today Event</h6>

            @if (isset($cruuentEvent->id))

                <div class=" pb-1 mb-3 py-4 my-4">
                    <img src="assets/img/avatars/9-small.png" class="d-block mx-auto ui-w-40 rounded-circle" alt>
                    <div class="media-body mt-4 ml-3">
                        <h4 class="font-weight-bold">
                            {{ $cruuentEvent->event_name }}
                            <div class="text-muted text-tiny mt-1"><small class="font-weight-normal">{{ $cruuentEvent->category->category_name }}</small></div>
                        </h4>
                        <!-- <a href="javascript:void(0)">Amanda Warner</a> -->
                        <span class="text-muted">Organized By</span>
                        <a href="javascript:void(0)">{{ Auth::user()->name ?? '-' }}</a>
                        <p class="my-1">{{ $cruuentEvent->description }}</p>
                    </div>
                </div>

            @else
                    <h1 class="text-center">
                         No Event Today
                    </h1>
            @endif

            <a href="{{ route('events.index') }}" class="card-footer d-block text-center text-body small font-weight-semibold">View all</a>
        </div>
        <!-- / Support tickets -->

    </div>
</div>
@endsection
