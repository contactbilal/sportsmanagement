@extends('layouts.app')

@section('content')
<div class="page-loader">
    <div class="bg-primary"></div>
</div>

<!-- Content -->

<div class="authentication-wrapper authentication-3">
    <div class="authentication-inner">

        <!-- Side container -->
        <!-- Do not display the container on extra small, small and medium screens -->
        <div class="d-none d-lg-flex col-lg-8 align-items-center ui-bg-cover ui-bg-overlay-container p-5" style="background-image: url({{ asset('admin/login/assets/img/admin-login-page.jpg') }});">
            <div class="ui-bg-overlay bg-dark opacity-50"></div>

            <!-- Text -->
            <div class="w-100 text-white px-5">
                <h1 class="display-2 font-weight-bolder mb-4">Admin LOGIN<br>Sport Managment System</h1>
                <div class="text-large font-weight-light">
                    (SMS)
                </div>
            </div>
            <!-- /.Text -->
        </div>
        <!-- / Side container -->

        <!-- Form container -->
        <div class="d-flex col-lg-4 align-items-center bg-white p-5">
            <!-- Inner container -->
            <!-- Have to add `.d-flex` to control width via `.col-*` classes -->
            <div class="d-flex col-sm-7 col-md-5 col-lg-12 px-0 px-xl-4 mx-auto">
                <div class="w-100">

                    <!-- Logo -->
                    <div class="d-flex justify-content-center align-items-center">
                        <div class="">
                            <div class="w-100 text-center">
                                <h1 class="text-dark">
                                    SMS-Expo
                                </h1>

                            </div>
                        </div>
                    </div>

                    <h4 class="text-center text-lighter font-weight-normal mt-5 mb-0">Login to Your Account</h4>


                    <!-- Form -->
                    <form method="POST" class="my-5"  action="{{ route('login') }}">
                        @csrf

                        <div class="form-group">
                            <label for="email" class="col-md-4 col-form-label">{{ __('E-Mail') }}</label>

                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group">
                            <label class="form-label d-flex justify-content-between align-items-end">
              <div>{{ __('Password') }}</div>

            </label>
                            <input id="password" type="password" class="form-control  @error('password') is-invalid @enderror " name="password" required autocomplete="current-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>



                        <div class="d-flex justify-content-between align-items-center m-0">

                            <button type="submit" class="btn btn-primary"> {{ __('Login') }}</button>
                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                             @endif
                        </div>
                    </form>
                    <!-- / Form -->

                </div>
            </div>
        </div>
        <!-- / Form container -->

    </div>
</div>

@endsection
