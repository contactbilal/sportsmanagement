@extends('layouts.website.site')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/step.css') }}">
@section('content')
<br><br>
<div class="container margin_60_35">
    <div class="row add_bottom_30">
        <div id="tabing" class="w-100">
            <div class="col-md-12">
                <form id="custom" method="POST" class="add_bottom_30">
                    <input id="website" name="website" type="text" value="">
                    <!-- Leave for security protection, read docs for details. Delete this comment before to publish. -->
                    <fieldset title="Review Your Items">
                        <legend></legend>
                        <h3 class="red_tab">1. BILLING DETAILS:</h3>
                        <div class="col-md-8 bg_tab_border">
                            <div class="col-md-12 tab_1_space mb-5 row ">
                                <div class="col-sm-12 col-md-1"> </div>
                                <div class="col-md-11">
                                    <h5 class="control-label" for="full_name">Name:</h5>
                                    <span class="caption"><input type="text" class="form-control onlyLetters valid" name="full_name" id="full_name" placeholder="First Name" aria-required="true" aria-invalid="false">
                                  <label class="first_name_error custom_error"></label></span>
                                </div>
                            </div>
                            <div class="col-md-12 tab_1_space mb-5 row">
                                <div class="col-sm-12 col-md-1"> </div>
                                <div class="col-md-11">
                                    <h5 class="control-label" for="book_seats">Book Seats:</h5>
                                    <span class="caption"><input type="number" class="form-control onlyLetters valid" name="book_seats" id="book_seats" placeholder="Total Seats" aria-required="true" aria-invalid="false">
                                  <label class="book_seats_error custom_error"></label></span>
                                </div>
                            </div>
                            <div class="col-md-12 tab_1_space mb-5 row">
                                <div class="col-md-1"><i class="fa fa-location-arrow"></i></div>
                                <div class="col-md-11">
                                    <span class="caption text-justify"><textarea type="text" class="form-control onlyLetters valid" name="address" id="address" placeholder="Address" cols="25" rows="5" aria-required="true" aria-invalid="false"></textarea>
                                  <label class="address_error custom_error"></label></span>
                                </div>
                            </div>
                            <div class="col-md-12 tab_1_space mb-5 row">
                                <div class="col-md-1"><i class="fas fa-mobile-alt"></i></div>
                                <div class="col-md-11">
                                    <span class="caption text-justify"><input type="text" class="form-control valid" name="mobile" id="mobile" placeholder="Mobile Number" aria-required="true" aria-invalid="false"></span>
                                </div>
                            </div>
                            <div class="col-md-12 tab_1_space mb-5 row">
                                <div class="col-md-1"><i class="fas fa-envelop"></i></div>
                                <div class="col-md-11">
                                    <span class="caption text-justify"><input type="email" class="form-control valid" name="email" id="email" placeholder="Email" aria-required="true" aria-invalid="false"></span>
                                </div>
                            </div>
                            <div class="col-md-12 tab_1_space mb-5 row">
                                <div class="col-sm-12 col-md-1"> </div>
                                <div class="col-md-11">
                                    <h5 class="control-label" for="comments">Add a comment or instruction to your order</h5>
                                    <span class="caption text-justify">
                                  <textarea cols="25" rows="5" name="comments" id="comments" class="form-control onlyLetters valid" aria-invalid="false"></textarea>
                                  <label class="comments_error custom_error"></label>
                                  </span>
                                </div>
                            </div>
                        </div><br>




                    </fieldset>
                    <!-- End Step one -->

                    <fieldset title="Payment">
                        <legend></legend>
                        <h3 class="red_tab">2. REVIEW AND CONFIRM YOUR ORDER</h3>
                        <div class="bg_tab_payment">
                            <div class="col-md-8">
                                <div class="row">

                                    <div class="col-md-3">
                                        <img src="assets/images/product_01.jpg" width="100px" height="100px">
                                    </div>
                                    <div class="col-md-3 text-left add-cart">
                                        <p>Event Tickets Only</p>
                                        <span class="price">USD $ 99</span>
                                        <br>
                                        <h4></h4>
                                    </div>
                                    <div class="col-md-2 quan-btn text-center">
                                        1 </div>
                                    <div class="col-md-4 quan-btn text-center">
                                        <span class="mr-2">
                                             <strong class="font black">USD $99.00</strong> 
                                        </span>
                                        <a class="" onclick="return confirm('Are you sure you want to remove this item in the cart?');" href="https://optimisedresumes.com/shop/remove_cart/eccbc87e4b5ce2fe28308fd9f2a7baf3"><i class="fa fa-times" aria-hidden="true"></i></a>

                                    </div>
                                </div>
                            </div>
                            <div id="promo_response">
                                <div class=" row mt_tab">
                                    <div class="offset-md-2 col-md-8">
                                        <div class="billing">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td>1 items</td>
                                                                <!--<td>$42.95</td>-->
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Total</strong></td>
                                                                <td><span id="total_container">USD $99.00</span>
                                                                    <input type="hidden" name="total" id="total">
                                                                </td>
                                                                <input type="hidden" name="subtotal" id="subtotal" value="99">
                                                            </tr>
                                                            <tr class="discount_row" style="display:none;">
                                                                <td class="empty-cell"> </td>
                                                                <td colspan="3"><b>Discount</b> <span id="discount_container"></span></td>
                                                                <td><span id="discount_amount"></span>
                                                                    <input type="hidden" name="discount" id="discount">
                                                                    <input type="hidden" name="promo_type" id="promo_type">
                                                                    <input type="hidden" name="promo_code" id="promo_code">
                                                                    <input type="hidden" name="promo_value" id="promo_value">
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!--COlmd12  -->
                                            </div>
                                            <!-- Row -->
                                        </div>
                                        <!-- Billing -->
                                    </div>
                                    <!-- Colmdoff8 and colmd4 -->
                                </div>
                                <section class="done">
                                    <div class="container">
                                        <div class="offset-md-1 col-md-8 add-cart" style="padding-left:0px;">
                                            <div class="row">
                                                <h3>Event Venue</h3>
                                                <h5 class="col-12">Lahore stadium</h5>
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3403.385318694425!2d74.27949351500696!3d31.458585057338286!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391901643a990297%3A0x8324f0d0661404f8!2sJohar%20Town%20Hockey%20Stadium!5e0!3m2!1sen!2s!4v1622102573808!5m2!1sen!2s"
                                                    width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                                            </div>
                                        </div>
                                        <!-- Colmdoff5 -->
                                    </div>
                                    <!-- Container -->
                                </section>
                            </div>

                        </div>



                    </fieldset>


                    <fieldset title="Done">
                        <legend></legend>
                        <h3 class="red_tab">3. PAYMENT METHOD</h3>
                        <div class="bg_tab_payment">
                            <div class="col-md-12 paymentTab">
                                <div class="form-group">
                                    <input type="radio" value="paypal" id="apartment" name="payment" class="icheck"><span class="lb">&nbsp;Paypal</span><span> <img src="https://optimisedresumes.com/assets/home/img/paypal.png" class="paypal-img" alt="paypal"></span>
                                </div>
                                <!--<div class="form-group">-->
                                <!--	<input type="radio" value="2checkout" id="villa" name="payment" class="icheck">-->
                                <!--	<span class="lb">2CheckOut</span><span> <img src="https://optimisedresumes.com/assets/home/img/2co.png" class="co"  alt="2CheckOut"></span>-->
                                <!--</div>-->
                                <div class="form-group">
                                    <input type="radio" value="stripe" id="office" name="payment" class="icheck"><span class="lb">&nbsp;Pay With Credit Card/Debit Card</span><span> </span>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <!-- End Step five -->
                    <input type="submit" class="finish" value="Finish!">
                </form>
            </div>
        </div>
    </div>
    <!-- End row -->
    <hr>
</div>
<script src="{{ asset('assets/js/jquery.stepy.js') }}"></script>
<script src="{{ asset('assets/js/quotation-validate.js') }}"></script>
<script src="{{ asset('assets/js/jquery.validate.js') }}"></script>
<script src="{{ asset('assets/js/custom-file-input.js') }}"></script>
@endsection