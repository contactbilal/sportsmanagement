@extends('layouts.website.site')
@section('content')

    <!-- Page Content -->
    <!-- Page Content -->
    <div class="barnner page-football-bg header-text">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-content text-center">
                        <h2>Football Event</h2>
                        <h4 class="mb-5">Buy Ticket Now</h4>
                        <a href="#" class="btn-blue mt-5 rounded-0">Buy Tickets</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Events Details bar Start here -->
    <div id="event-details-bar">
        <div class="container-fluid bg-defathemeult- bg-blue-theme">
            <div class="event-bar">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="d-flex py-5 justify-content-center">
                            <div class="d-inline-block  py-3 px-5 text-center">
                                <h5 class="text-white py-3">10</h5>
                                <h4 class="text-white text-uppercase font-ag-bold">Total Teams</h4>
                            </div>
                            <div class="d-inline-block border-left py-3 px-5 text-center">
                                <h5 class="text-white py-3">20</h5>
                                <h4 class="text-white text-uppercase font-ag-bold">Total Seats</h4>
                            </div>
                            <div class="d-inline-block border-left py-3 px-5 text-center">
                                <h5 class="text-white py-3">10</h5>
                                <h4 class="text-white text-uppercase font-ag-bold">Confirm Bookings</h4>
                            </div>
                            <div class="d-inline-block border-left py-3 px-5 text-center">
                                <h5 class="text-white py-3">10</h5>
                                <h4 class="text-white text-uppercase font-ag-bold">Active Matches</h4>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About Event -->
    <div class="best-features about-features">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>Our Background</h2>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="left-content">
                        <h4>Who we are &amp; What we do?</h4>
                        <p>Football, also called association football or soccer, is a game involving two teams of 11 players who try to maneuver the ball into the other team's goal without using their hands or arms. The team that scores more goals wins. Football is the world's most popular ball game in numbers of participants and spectators<br><br>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, consequuntur, modi mollitia corporis ipsa voluptate corrupti eum ratione ex ea praesentium
                            quibusdam? Aut, in eum facere corrupti necessitatibus perspiciatis quis.</p>
                        <a href="#" class="btn-blue rounded-0">Buy Tickets</a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="right-image">
                        <img src="assets/images/ft.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- latest teams -->
    <div id="team-table" class="football mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>Event Teams</h2>
                    </div>
                </div>
                <div class="p-10 bg-surface-secondary">
                    <div class="container">
                        <div class="card">
                            <div class="table-responsive">
                                <table class="table table-hover table-nowrap">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope=" ">Country</th>
                                            <th scope=" ">City</th>
                                            <th scope=" ">State</th>
                                            <th scope=" ">Team Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td data-label="profile">
                                                <img alt="..." src="assets/images/pakistan.png" width="75" height="70" class="avatar avatar-sm rounded-circle me-2">
                                            </td>
                                            <td data-label="city">
                                                <span>Rawalpindi</span>
                                            </td>
                                            <td data-label="Phone">
                                                <a class="text-current" href="#">Punjab</a>
                                            </td>
                                            <td data-label="Lead Score">
                                                <a class="text-current" href="tel:202-555-0152">Team A</a>
                                            </td>


                                        </tr>
                                        <tr>
                                            <td data-label="profile">
                                                <img alt="..." src="assets/images/pakistan.png" width="75" height="70" class="avatar avatar-sm rounded-circle me-2">
                                            </td>
                                            <td data-label="city">
                                                <span>Rawalpindi</span>
                                            </td>
                                            <td data-label="Phone">
                                                <a class="text-current" href="#">Punjab</a>
                                            </td>
                                            <td data-label="Lead Score">
                                                <a class="text-current" href="tel:202-555-0152">Team B</a>
                                            </td>


                                        </tr>
                                        <tr>
                                            <td data-label="profile">
                                                <img alt="..." src="assets/images/pakistan.png" width="75" height="70" class="avatar avatar-sm rounded-circle me-2">
                                            </td>
                                            <td data-label="city">
                                                <span>Rawalpindi</span>
                                            </td>
                                            <td data-label="Phone">
                                                <a class="text-current" href="#">Punjab</a>
                                            </td>
                                            <td data-label="Lead Score">
                                                <a class="text-current" href="tel:202-555-0152">Team C</a>
                                            </td>


                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- call to action -->
    <section id="action" class="actionfootball">
        <div class="container aos-init aos-animate" data-aos="zoom-in">
            <div class="rwo d-md-flex justify-content-between align-items-center">
                <div class="section-header col-md-7">
                    <h2 class="text-white font-ag-bold">Rerum numquam illum recusandae quia mollitia consequatur.</h2>
                </div>
                <div>
                    <form method="POST" action="#">
                        <div class="form-row justify-content-center">
                            <div class="col-auto">
                                <a href="#" class="filled-dark-button  btn-lg rounded-0">Buy Tickets</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Player Squad -->
    <div id="team-table" class="football mt-5 pt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>Players Squad</h2>
                    </div>
                </div>
                <!-- Demo header-->
                <section class="  header">
                    <div class="container ">
                        <div class="row">
                            <div class="col-md-3">
                                <!-- Tabs nav -->
                                <div class="nav flex-column nav-pills nav-pills-custom" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    <a class="nav-link mb-3 p-3 shadow active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                                        <i class="fa fa-star mr-2"></i>
                                        <span class="font-weight-bold small text-uppercase">Team A</span></a>

                                    <a class="nav-link mb-3 p-3 shadow" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                                        <i class="fa fa-star mr-2"></i>
                                        <span class="font-weight-bold small text-uppercase">Team B</span></a>

                                    <a class="nav-link mb-3 p-3 shadow" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">
                                        <i class="fa fa-star mr-2"></i>
                                        <span class="font-weight-bold small text-uppercase">Team C</span></a>


                                </div>
                            </div>


                            <div class="col-md-9">
                                <!-- Tabs content -->
                                <div class="tab-content" id="v-pills-tabContent">
                                    <div class="tab-pane fade shadow rounded bg-white show active p-5" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-nowrap">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th scope=" ">Profile</th>
                                                        <th scope=" ">Name</th>
                                                        <th scope=" ">Country</th>
                                                        <th scope=" ">City</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td data-label="profile">
                                                            <img alt="..." src="assets/images/player-squad.jpg" width="75" height="70" class="avatar avatar-sm rounded-circle me-2">
                                                        </td>
                                                        <td data-label="city">
                                                            <span>Buttler</span>
                                                        </td>
                                                        <td data-label="Phone">
                                                            England
                                                        </td>
                                                        <td data-label="Lead Score">
                                                            London
                                                        </td>


                                                    </tr>
                                                    <tr>
                                                        <td data-label="profile">
                                                            <img alt="..." src="assets/images/player-squad.jpg" width="75" height="70" class="avatar avatar-sm rounded-circle me-2">
                                                        </td>
                                                        <td data-label="city">
                                                            <span>Buttler</span>
                                                        </td>
                                                        <td data-label="Phone">
                                                            England
                                                        </td>
                                                        <td data-label="Lead Score">
                                                            London
                                                        </td>


                                                    </tr>
                                                    <tr>
                                                        <td data-label="profile">
                                                            <img alt="..." src="assets/images/player-squad.jpg" width="75" height="70" class="avatar avatar-sm rounded-circle me-2">
                                                        </td>
                                                        <td data-label="city">
                                                            <span>Buttler</span>
                                                        </td>
                                                        <td data-label="Phone">
                                                            England
                                                        </td>
                                                        <td data-label="Lead Score">
                                                            London
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-nowrap">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th scope=" ">Profile</th>
                                                        <th scope=" ">Name</th>
                                                        <th scope=" ">Country</th>
                                                        <th scope=" ">City</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td data-label="profile">
                                                            <img alt="..." src="assets/images/player-squad.jpg" width="75" height="70" class="avatar avatar-sm rounded-circle me-2">
                                                        </td>
                                                        <td data-label="city">
                                                            <span>Buttler</span>
                                                        </td>
                                                        <td data-label="Phone">
                                                            England
                                                        </td>
                                                        <td data-label="Lead Score">
                                                            London
                                                        </td>


                                                    </tr>
                                                    <tr>
                                                        <td data-label="profile">
                                                            <img alt="..." src="assets/images/player-squad.jpg" width="75" height="70" class="avatar avatar-sm rounded-circle me-2">
                                                        </td>
                                                        <td data-label="city">
                                                            <span>Buttler</span>
                                                        </td>
                                                        <td data-label="Phone">
                                                            England
                                                        </td>
                                                        <td data-label="Lead Score">
                                                            London
                                                        </td>


                                                    </tr>
                                                    <tr>
                                                        <td data-label="profile">
                                                            <img alt="..." src="assets/images/player-squad.jpg" width="75" height="70" class="avatar avatar-sm rounded-circle me-2">
                                                        </td>
                                                        <td data-label="city">
                                                            <span>Buttler</span>
                                                        </td>
                                                        <td data-label="Phone">
                                                            England
                                                        </td>
                                                        <td data-label="Lead Score">
                                                            London
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td data-label="profile">
                                                            <img alt="..." src="assets/images/player-squad.jpg" width="75" height="70" class="avatar avatar-sm rounded-circle me-2">
                                                        </td>
                                                        <td data-label="city">
                                                            <span>Buttler</span>
                                                        </td>
                                                        <td data-label="Phone">
                                                            England
                                                        </td>
                                                        <td data-label="Lead Score">
                                                            London
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-nowrap">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th scope=" ">Profile</th>
                                                        <th scope=" ">Name</th>
                                                        <th scope=" ">Country</th>
                                                        <th scope=" ">City</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td data-label="profile">
                                                            <img alt="..." src="assets/images/player-squad.jpg" width="75" height="70" class="avatar avatar-sm rounded-circle me-2">
                                                        </td>
                                                        <td data-label="city">
                                                            <span>Buttler</span>
                                                        </td>
                                                        <td data-label="Phone">
                                                            England
                                                        </td>
                                                        <td data-label="Lead Score">
                                                            London
                                                        </td>


                                                    </tr>
                                                    <tr>
                                                        <td data-label="profile">
                                                            <img alt="..." src="assets/images/player-squad.jpg" width="75" height="70" class="avatar avatar-sm rounded-circle me-2">
                                                        </td>
                                                        <td data-label="city">
                                                            <span>Buttler</span>
                                                        </td>
                                                        <td data-label="Phone">
                                                            England
                                                        </td>
                                                        <td data-label="Lead Score">
                                                            London
                                                        </td>


                                                    </tr>
                                                    <tr>
                                                        <td data-label="profile">
                                                            <img alt="..." src="assets/images/player-squad.jpg" width="75" height="70" class="avatar avatar-sm rounded-circle me-2">
                                                        </td>
                                                        <td data-label="city">
                                                            <span>Buttler</span>
                                                        </td>
                                                        <td data-label="Phone">
                                                            England
                                                        </td>
                                                        <td data-label="Lead Score">
                                                            London
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- ======= Schedule Section ======= -->
    <section id="schedule" class="schedulefootball section-with-bg mt-5">
        <div class="container" data-aos="fade-up">
            <div class="section-heading">
                <h2 class="text-white">Event Matches</h2>
            </div>

            <ul class="nav nav-tabs" role="tablist" data-aos="fade-up" data-aos-delay="100">
                <li class="nav-item">
                    <a class="nav-link active" href="#day-1" role="tab" data-toggle="tab">Match</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#day-2" role="tab" data-toggle="tab">Upcomming Match</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#day-3" role="tab" data-toggle="tab">Winner</a>
                </li>
            </ul>

            <h3 class="sub-heading text-white">Voluptatem nulla veniam soluta et corrupti consequatur neque eveniet officia. Eius necessitatibus voluptatem quis labore perspiciatis quia.</h3>

            <div class="tab-content row justify-content-center" data-aos="fade-up" data-aos-delay="200">

                <!-- Schdule Day 1 -->
                <div role="tabpanel" class="col-lg-10 tab-pane fade show active" id="day-1">
                    <div class="row schedule-item align-items-center">
                        <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                        <div class="col-md-7">
                            <div class="wrap d-md-flex align-items-center justify-content-between">
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>
                                <div>
                                    <h1 class="text-white ">VS</h1>
                                </div>
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <a href="event.html " class="filled-dark-button rounded-0 ">Buy Tickets</a>

                        </div>
                    </div>

                    <div class="row schedule-item align-items-center">
                        <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                        <div class="col-md-7">
                            <div class="wrap d-md-flex align-items-center justify-content-between">
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>
                                <div>
                                    <h1 class="text-white ">VS</h1>
                                </div>
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <a href="event.html " class="filled-dark-button rounded-0 ">Buy Tickets</a>

                        </div>
                    </div>

                    <div class="row schedule-item align-items-center">
                        <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                        <div class="col-md-7">
                            <div class="wrap d-md-flex align-items-center justify-content-between">
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>
                                <div>
                                    <h1 class="text-white ">VS</h1>
                                </div>
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <a href="event.html " class="filled-dark-button rounded-0 ">Buy Tickets</a>

                        </div>
                    </div>

                    <div class="row schedule-item align-items-center">
                        <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                        <div class="col-md-7">
                            <div class="wrap d-md-flex align-items-center justify-content-between">
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>
                                <div>
                                    <h1 class="text-white ">VS</h1>
                                </div>
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <a href="event.html " class="filled-dark-button rounded-0 ">Buy Tickets</a>

                        </div>
                    </div>

                    <div class="row schedule-item align-items-center">
                        <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                        <div class="col-md-7">
                            <div class="wrap d-md-flex align-items-center justify-content-between">
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>
                                <div>
                                    <h1 class="text-white ">VS</h1>
                                </div>
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <a href="event.html " class="filled-dark-button rounded-0 ">Buy Tickets</a>

                        </div>
                    </div>

                    <div class="row schedule-item align-items-center">
                        <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                        <div class="col-md-7">
                            <div class="wrap d-md-flex align-items-center justify-content-between">
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>
                                <div>
                                    <h1 class="text-white ">VS</h1>
                                </div>
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <a href="event.html " class="filled-dark-button rounded-0 ">Buy Tickets</a>

                        </div>
                    </div>

                </div>
                <!-- End Schdule Day 1 -->

                <!-- Schdule Day 2 -->
                <div role="tabpanel " class="col-lg-9 tab-pane fade " id="day-2">

                    <div class="row schedule-item align-items-center">
                        <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                        <div class="col-md-7">
                            <div class="wrap d-md-flex align-items-center justify-content-between">
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>
                                <div>
                                    <h1 class="text-white ">VS</h1>
                                </div>
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <a href="event.html " class="filled-dark-button rounded-0 ">Buy Tickets</a>

                        </div>
                    </div>

                    <div class="row schedule-item align-items-center">
                        <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                        <div class="col-md-7">
                            <div class="wrap d-md-flex align-items-center justify-content-between">
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>
                                <div>
                                    <h1 class="text-white ">VS</h1>
                                </div>
                                <div>
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                                    </div>
                                    <br><br><br>
                                    <h6 class="text-white ">Team Name</h6>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <a href="event.html " class="filled-dark-button rounded-0 ">Buy Tickets</a>

                        </div>
                    </div>
                </div>
                <!-- End Schdule Day 2 -->

                <!-- Schdule Day 3 -->
                <div role="tabpanel " class="col-lg-9 tab-pane fade " id="day-3">

                    <div class="row schedule-item ">
                        <div class="col-md-2 ">
                            <h6 class="text-white">1st Match</h6> <em class="text-white "> 21 May,2021</em> </div>
                        <div class="col-md-10 ">
                            <div class="speaker ">
                                <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                            </div>
                            <h4 class="text-white">Team Name</h4>
                            <p>Winner</p>

                        </div>
                    </div>

                    <div class="row schedule-item ">
                        <div class="col-md-2 ">
                            <h6 class="text-white">1st Match</h6> <em class="text-white "> 21 May,2021</em> </div>
                        <div class="col-md-10 ">
                            <div class="speaker ">
                                <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                            </div>
                            <h4 class="text-white">Team Name</h4>
                            <p>Winner</p>

                        </div>
                    </div>

                    <div class="row schedule-item ">
                        <div class="col-md-2 ">
                            <h6 class="text-white">1st Match</h6> <em class="text-white "> 21 May,2021</em> </div>
                        <div class="col-md-10 ">
                            <div class="speaker ">
                                <img src="assets/images/product_01.jpg " alt="Brenden Legros ">
                            </div>
                            <h4 class="text-white">Team Name</h4>
                            <p>Winner</p>

                        </div>
                    </div>

                </div>
                <!-- End Schdule Day 2 -->

            </div>

        </div>

    </section>
    <!-- End Schedule Section -->
    <!-- latest News -->
    <div id="team-table" class="football mt-5">
        <div class="container ">
            <div class="row ">
                <div class="col-md-12 ">
                    <div class="section-heading ">
                        <h2>Event Winner's</h2>

                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover table-nowrap">
                        <thead class="thead-light">
                            <tr>
                                <th scope=" ">Position</th>
                                <th scope=" ">Team Name</th>
                                <th scope=" ">Country</th>
                                <th scope=" ">City</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-label="profile">
                                    1st
                                </td>
                                <td data-label="city">
                                    <span>Team Name</span>
                                </td>
                                <td data-label="Phone">
                                    England
                                </td>
                                <td data-label="Lead Score">
                                    London
                                </td>


                            </tr>
                            <tr>
                                <td data-label="profile">
                                    2nd
                                </td>
                                <td data-label="city">
                                    <span>Team Name</span>
                                </td>
                                <td data-label="Phone">
                                    England
                                </td>
                                <td data-label="Lead Score">
                                    London
                                </td>


                            </tr>
                            <tr>
                                <td data-label="profile">
                                    3rd
                                </td>
                                <td data-label="city">
                                    <span>Team Name</span>
                                </td>
                                <td data-label="Phone">
                                    England
                                </td>
                                <td data-label="Lead Score">
                                    London
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>
@endsection