@extends('layouts.website.site')
@section('content')
<div class="page-heading page-title-bg header-text" style="height: 200px!important;padding:unset!important">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-content text-left">
                   <strong  class="text-white" style="    position: absolute;
                   top: 129px;
                   font-size: 28px;">
                       Dashboard / Edit
                   </strong>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="profile-sidebar shadow">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic text-center">
                        <img src="https://gravatar.com/avatar/31b64e4876d603ce78e04102c67d6144?s=80&d=https://codepen.io/assets/avatars/user-avatar-80x80-bdcd44a3bfb9a5fd01eb8b86f9e033fa1a9897c3a15b33adfc2649a002dab1b6.png" class="mx-auto" alt="">
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            Jason Davis
                        </div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <div class="profile-userbuttons">
                        <button type="button" class="btn btn-success btn-sm">Edit Info</button>
                        <!-- <button type="button" class="btn btn-danger btn-sm">Settings</button> -->
                    </div>
                    <!-- END SIDEBAR BUTTONS -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li class="active w-100">
                                <a href="#" class="w-100">
                                    <i class="glyphicon glyphicon-home"></i> Overview
                                </a>
                            </li>
                            <li class="w-100">
                                <a href="#" class="w-100">
                                    <i class="glyphicon glyphicon-user"></i> Account Settings
                                </a>
                            </li>
                            <!-- <li class="w-100">
                                <a href="#" target="_blank">
                                    <i class="glyphicon glyphicon-ok"></i> Tasks </a>
                            </li>
                            <li class="w-100">
                                <a href="#">
                                    <i class="glyphicon glyphicon-flag"></i> Help </a>
                            </li> -->
                        </ul>
                    </div>
                    <!-- END MENU -->

                    <div class="portlet light bordered">
                        <!-- STAT -->
                        <!-- <div class="row list-separated profile-stat">
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="uppercase profile-stat-title"> 37 </div>
                                <div class="uppercase profile-stat-text"> Projects </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="uppercase profile-stat-title"> 51 </div>
                                <div class="uppercase profile-stat-text"> Tasks </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="uppercase profile-stat-title"> 61 </div>
                                <div class="uppercase profile-stat-text"> Uploads </div>
                            </div>
                        </div> -->
                        <!-- END STAT -->
                        <div>
                            <!-- <h4 class="profile-desc-title">About Jason Davis</h4>
                            <span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span> -->
                            <div class="margin-top-20 profile-desc-link">
                                <i class="fa fa-envelope"></i>
                                <a href="https://www.apollowebstudio.com">apollowebstudio.com</a>
                            </div>
                            <div class="margin-top-20 profile-desc-link">
                                <i class="fa fa-phone"></i>
                                <a href="https://www.twitter.com/jasondavisfl/">@jasondavisfl</a>
                            </div>

                        </div>
                    </div>



                </div>
            </div>
            <div class="col-md-9 shadow">
                <div class="profile-content Edit Details">
                   <h4>
                       Edit
                   </h4>
                   <form method="post" action=" ">


                    <div class="card-body">
                        {{--  @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif  --}}
                        {{--  <div class="media align-items-center">
                            <img src="assets/img/avatars/5-small.png" alt="" class="d-block ui-w-80">
                            <div class="media-body ml-3">
                                <label class="form-label d-block mb-2">Avatar</label>
                                <label class="btn btn-outline-primary btn-sm">
          Change
          <input type="file" class="user-edit-fileinput">
        </label>&nbsp;
                                <button type="button" class="btn btn-default btn-sm md-btn-flat">Reset</button>
                            </div>
                        </div>

                    </div>  --}}
                    <hr class="border-light m-0">
                    <div class="card-body pb-2">

                        <div class="form-group">
                            <label class="form-label">Username</label>
                            <input type="text" class="form-control mb-1" name="user_name" placeholder="User Name"  >
                        </div>

                        <div class="form-group">
                            <label class="form-label">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Name"  >
                        </div>
                        <div class="form-group">
                            <label class="form-label">E-mail</label>
                            <input type="text" class="form-control mb-1" name="email" placeholder="example@exe.com"  >
                            {{-- <a href="javascript:void(0)" class="small">Resend confirmation</a> --}}
                        </div>
                        <div class="form-group">
                            <label class="form-label">Phone</label>
                            <input type="text" class="form-control" name="phone" placeholder="Phone"  >
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">Country</label>
                                    <input type="text" class="form-control" name="country" placeholder="City"  >
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">City</label>
                                    <input type="text" class="form-control" name="city" placeholder="City"  >
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">State</label>
                                    <input type="text" class="form-control" name="state" placeholder="state" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-right m-3 px-3">
                        <button type="submit" class="btn btn-primary">Save changes</button>&nbsp;
                        {{--  <a href="{{ route('users.index') }}" class="btn btn-default">Back</a>  --}}
                    </div>
                </form>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
