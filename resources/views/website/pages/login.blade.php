@extends('layouts.website.site')
<link rel="stylesheet" href="{{ asset('assets/Login/css/css-material-design-iconic-font.min.css') }}">

<!-- Main css -->
<link rel="stylesheet" href="{{ asset('assets/Login/css/css-style.css') }}">
@section('content')
   <!-- Sign up form -->
   <section class="sign-in">
    <div class="container bg-white shadow">
        <div class="signin-content">
            <div class="signin-image">
                <figure><img src="assets/images/unnamed.png" alt="sing up image"></figure>
                <a href="#" class="signup-image-link">Create an account</a>
            </div>

            <div class="signin-form">
                <h2 class="form-title">Sign In</h2>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form method="POST" class="my-5"  action="{{ route('website.login') }}">
                    @csrf

                    <div class="form-group">
                        <label for="email"><i class="zmdi zmdi-account material-icons-name"></i></label>

                        {{--  <label for="email" class="col-md-4 col-form-label">{{ __('E-Mail') }}</label>  --}}

                        <input id="email" type="email" class=" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                    </div>
                    <div class="form-group">
                        <label for="your_pass"><i class="zmdi zmdi-lock"></i></label>
                        <input id="password" type="password" class="  @error('password') is-invalid @enderror " name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>



                    <div class="d-flex justify-content-between align-items-center m-0">

                        <button type="submit" class="form-submit"> {{ __('Login') }}</button>
                        @if (Route::has('password.request'))
                            {{--  <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>  --}}
                         @endif
                    </div>
                </form>
                {{--  <div class="social-login">
                    <span class="social-label">Or login with</span>
                    <ul class="socials">
                        <li><a href="#"><i class="display-flex-center zmdi zmdi-facebook"></i></a></li>
                        <li><a href="#"><i class="display-flex-center zmdi zmdi-twitter"></i></a></li>
                        <li><a href="#"><i class="display-flex-center zmdi zmdi-google"></i></a></li>
                    </ul>
                </div>  --}}
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('assets/Login/js/393-js-main.js') }}"></script>
@endsection
