@extends('layouts.website.site')
@section('content')

    <!-- Page Content -->
    <div class="page-heading page-title-bg header-text">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-content text-left">
                        <h2>News</h2>
                        <h4>{{ $news->title }}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner Ends Here -->



    <!-- ======= Schedule Section ======= -->
    <section id="newsDetail" class="section-with-bg mt-5">
        <div class="container" data-aos="fade-up">
            <div class="col-md-12">
                <div class="section-heading d-flex justify-content-between">
                    <h2>{{ $news->title }}</h2>
                    <a>
                        @if(isset($news->created_at))
                            {{ $news->created_at->format('d M,Y') }}
                        @else
                           <span>-</span>
                        @endif
                    </a>
                </div>

                <img src="{{ asset('assets/images/product_02.jpg') }}" class="img-fluid mt-4 mb-3"  alt="">
                <p>{{ $news->description }}</p>
            </div>
        </div>
    </section>

@endsection
