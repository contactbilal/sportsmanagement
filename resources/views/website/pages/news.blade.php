@extends('layouts.website.site')
@section('content')
 <!-- Page Content -->
 <div class="page-heading page-title-bg header-text">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-content text-left">
                    <h2>Sports Event News</h2>
                    <h4>Our Latest News</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Banner Ends Here -->
<!-- Events Details bar Start here -->
<div id="event-details-bar">
    <div class="container-fluid bg-default-theme ">
        <div class="event-bar">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="d-flex py-5 justify-content-center">
                        <div class="d-inline-block  py-3 px-5 text-center">
                            <h5 class="text-white py-3">{{ $totalEvent ?? '-' }}</h5>
                            <h4 class="text-white text-uppercase font-ag-bold">Total Event</h4>
                        </div>
                        <div class="d-inline-block border-left py-3 px-5 text-center">
                            <h5 class="text-white py-3">{{ $DoneEvent ?? '0' }}</h5>
                            <h4 class="text-white text-uppercase font-ag-bold">Done Event</h4>
                        </div>
                        <div class="d-inline-block border-left py-3 px-5 text-center">
                            <h5 class="text-white py-3">{{ $totalUpcomingEvents ?? '0' }}</h5>
                            <h4 class="text-white text-uppercase font-ag-bold">Upcomming Event</h4>
                        </div>
                        <div class="d-inline-block border-left py-3 px-5 text-center">
                            <h5 class="text-white py-3">{{ $currentEvent ?? '0' }}</h5>
                            <h4 class="text-white text-uppercase font-ag-bold">Active Event</h4>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End Schedule Section -->
<!-- latest News -->
<div class="latest-events">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>Latest News</h2>
                    <a href="{{  url('/news') }}">view all news <i class="fa fa-angle-right"></i></a>
                </div>
            </div>

            @isset($allNewsShow)
            @if (count($allNewsShow)>0)
                @foreach ($allNewsShow as $latestnewslist)

                    <div class="col-md-4">

                            <div class="product-item">
                                <a href="{{ route('show.news.detail.page',$latestnewslist->id) }}"><img src="assets/images/product_02.jpg" class="img-fluid" height="150" alt=""></a>
                                <div class="down-content">
                                    <a href="{{ route('show.news.detail.page',$latestnewslist->id) }}">
                                        <h4 class="text-red">{{ $latestnewslist->title }}</h4>
                                    </a>

                                    <p>
                                        <a href="{{ route('show.news.detail.page',$latestnewslist->id) }}" class="text-dark">
                                        {{ $latestnewslist->description }}

                                        </a>
                                    </p>

                                    <span>
                                        @if(isset($latestnewslist->created_at))
                                             {{ $latestnewslist->created_at->format('d M,Y') }}
                                        @else
                                                <span>-</span>
                                        @endif
                                    </span>
                                </div>
                            </div>


                    </div>

                @endforeach
            @else
                <div class="mt-3">
                    <h2  class="text-red">
                        No Latest event
                    </h2>
                </div>
            @endif
        @endisset
        </div>
    </div>
</div>
@endsection
