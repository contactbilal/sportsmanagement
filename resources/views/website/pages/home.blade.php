   @extends('layouts.website.site')
   @section('content')
   <!-- Page Content -->
    <!-- Banner Starts Here -->
    <div class="banner header-text">
        <div class="owl-banner owl-carousel">
            <div class="banner-item-01">
                <div class="text-content col-md-5">
                    <h2>New Event <br> Comming Soon</h2>
                    <h4>Join Now to Enjoy</h4>
                    <a href="registerNow.html" class="filled-button">Create Event</a>
                </div>
            </div>
            <div class="banner-item-02">
                <div class="text-content">
                    <h2> Best Location <br> for Sports</h2>
                    <h4>Join Now to Enjoy</h4>
                    <a href="registerNow.html" class="filled-button">Create Event</a>
                </div>
            </div>
            <div class="banner-item-03">
                <div class="text-content">
                    <h2> Multiple event <br> Sport</h2>
                    <h4>Join Now to Enjoy</h4>
                    <a href="registerNow.html" class="filled-button">Create Event</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner Ends Here -->
    <!-- Events Details bar Start here -->
    <div id="event-details-bar">
        <div class="container-fluid bg-default-theme ">
            <div class="event-bar">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="d-flex py-5 justify-content-center">
                            <div class="d-inline-block  py-3 px-5 text-center">
                                <h5 class="text-white py-3">{{ $totalEvent ?? '-' }}</h5>
                                <h4 class="text-white text-uppercase font-ag-bold">Total Event</h4>
                            </div>
                            <div class="d-inline-block border-left py-3 px-5 text-center">
                                <h5 class="text-white py-3">{{ $DoneEvent ?? '0' }}</h5>
                                <h4 class="text-white text-uppercase font-ag-bold">Done Event</h4>
                            </div>
                            <div class="d-inline-block border-left py-3 px-5 text-center">
                                <h5 class="text-white py-3">{{ $totalUpcomingEvents ?? '0' }}</h5>
                                <h4 class="text-white text-uppercase font-ag-bold">Upcomming Event</h4>
                            </div>
                            <div class="d-inline-block border-left py-3 px-5 text-center">
                                <h5 class="text-white py-3">{{ $currentEvent ?? '0' }}</h5>
                                <h4 class="text-white text-uppercase font-ag-bold">Active Event</h4>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About Event -->
    <div class="best-features about-features">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>Our Background</h2>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="left-content">
                        <h4>Who we are &amp; What we do?</h4>
                        <p>Some of the most important events on the sporting calendar are competitions involving many sports, including the most popular sporting events Games. There are many more of these multi-sport events than most people would know about,
                            particularly those that cater for athletes from a particular region or from a particular group.
                        <br>
                        For the sports fan, the major sporting events that only come around once or year or every few years are highly anticipated, and worth waiting for. There are major events held for each of the hundreds of major sports around the world.
                        </p>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="right-image">
                        <img src="{{ asset('assets/images/unnamed.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Events Details bar End Here -->
    <div class="latest-sports">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2 class="text-white">Event Sports</h2>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="product-item">
                        <a href="#"><img src="assets/images/martialarts.png" alt=""></a>
                        <div class="down-content text-center">
                            <a href="ufcEvent.html">
                                <h4 class="links-dark-red">View Events</h4>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="product-item">
                        <a href="#"><img src="assets/images/football.png" alt=""></a>
                        <div class="down-content text-center">
                            <a href="footballEvent.html">
                                <h4 class="links-dark-red">View Events</h4>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="product-item">
                        <a href="#"><img src="assets/images/cricket_PNG55.png" alt=""></a>
                        <div class="down-content text-center">
                            <a href="cricketEvent.html">
                                <h4 class="links-dark-red">View Events</h4>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- latest Event -->
    <div class="latest-products mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>Latest Events</h2>
                    </div>
                </div>
                @isset($latestEvents)
                    @if (count($latestEvents)>0)
                        @foreach ($latestEvents as $latestevent)

                            <div class="col-md-4">

                                    <div class="product-item">
                                        <a href="{{ route('event.category.page', $latestevent->id)}}"><img src="assets/images/product_02.jpg" class="img-fluid" height="150" alt=""></a>
                                        <div class="down-content">
                                            <a href="{{ route('event.category.page', $latestevent->id) }}">
                                                <h4 class="text-red">{{ $latestevent->event_name }}</h4>
                                            </a>
                                            <p>{{ $latestevent->description }}</p>

                                            <span>
                                                @if(isset($latestevent->created_at))
                                                     {{ $latestevent->created_at->format('d M,Y') }}
                                                @else
                                                        <span>-</span>
                                                @endif
                                            </span>
                                        </div>
                                    </div>


                            </div>

                        @endforeach
                    @else
                        <div class="mt-3">
                            <h2  class="text-red">
                                No Latest event
                            </h2>
                        </div>
                    @endif
                @endisset

            </div>
        </div>
    </div>

    <!-- ======= Schedule Section ======= -->
    <section id="schedule" class="section-with-bg mt-5">
        <div class="container" data-aos="fade-up">
            <div class="section-heading">
                <h2 class="text-white">Event Schedule</h2>
            </div>

           @isset($allCategories)
                @if (count($allCategories)>0)
                    <ul class="nav nav-tabs" role="tablist" data-aos="fade-up" data-aos-delay="100">
                        @foreach ($allCategories as $categoryList)
                           @if ($categoryList->id == 1)
                        <li class="nav-item">
                            <a class="nav-link active" href="#home{{ $categoryList->id }}" role="tab" data-toggle="tab">{{ $categoryList->category_name }}</a>
                        </li>
                           @else
                           <li class="nav-item">
                            <a class="nav-link  " href="#home{{ $categoryList->id }}" role="tab" data-toggle="tab">{{ $categoryList->category_name }}</a>
                        </li>
                           @endif
                       @endforeach
                    </ul>
                @else
                    no item Found
                @endif
                <h3 class="sub-heading text-white">See all sports  events schedule <br> join a favorit event </h3>

                <div class="tab-content row justify-content-center" data-aos="fade-up" data-aos-delay="200">

                    <!-- Schdule-->
                    @foreach ($allCategories as $item)
                    @if ($item->id == 1)

                        <div role="tabpanel" class="col-lg-9 tab-pane fade show active" id="home{{ $item->id }}">
                            <div class="row schedule-item">
                                <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                                <div class="col-md-10">
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg" alt="Brenden Legros">
                                    </div>
                                    <h4>Keynote <span>Brenden Legros</span></h4>
                                    <p>Facere provident incidunt quos voluptas.</p>
                                    <a href="event.html" class="filled-button rounded-0">View Details</a>
                                </div>
                            </div>
                        </div>

                    @else

                        <div role="tabpanel" class="col-lg-9 tab-pane fade" id="home{{ $item->id }}">
                            <div class="row schedule-item">
                                <div class="col-md-2"><time>10:00 AM</time><br> <em class="text-white"> 21 May,2021</em> </div>
                                <div class="col-md-10">
                                    <div class="speaker">
                                        <img src="assets/images/product_01.jpg" alt="Brenden Legros">
                                    </div>
                                    <h4>Keynote <span>Brenden Legros</span></h4>
                                    <p>Facere provident incidunt quos voluptas.</p>
                                    <a href="event.html" class="filled-button rounded-0">View Details</a>
                                </div>
                            </div>
                        </div>

                    @endif
                    @endforeach

                    <!-- End Schdule  -->
                </div>
           @endisset



        </div>

    </section>
    <!-- End Schedule Section -->
    <!-- latest News -->
    <div class="latest-events">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>Latest News</h2>
                        <a href="{{  url('/news') }}">view all news <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>

                @isset($latestNews)
                @if (count($latestNews)>0)
                    @foreach ($latestNews as $latestnewslist)

                        <div class="col-md-4">

                                <div class="product-item">
                                    <a href="{{ route('show.news.detail.page',$latestnewslist->id) }}"><img src="assets/images/product_02.jpg" class="img-fluid" height="150" alt=""></a>
                                    <div class="down-content">
                                        <a href="{{ route('show.news.detail.page',$latestnewslist->id) }}">
                                            <h4 class="text-red">{{ $latestnewslist->title }}</h4>
                                        </a>

                                        <p>
                                            <a href="{{ route('show.news.detail.page',$latestnewslist->id) }}" class="text-dark">
                                            {{ $latestnewslist->description }}

                                            </a>
                                        </p>

                                        <span>
                                            @if(isset($latestnewslist->created_at))
                                                 {{ $latestnewslist->created_at->format('d M,Y') }}
                                            @else
                                                    <span>-</span>
                                            @endif
                                        </span>
                                    </div>
                                </div>


                        </div>

                    @endforeach
                @else
                    <div class="mt-3">
                        <h2  class="text-red">
                            No Latest event
                        </h2>
                    </div>
                @endif
            @endisset
            </div>
        </div>
    </div>
    @endsection
