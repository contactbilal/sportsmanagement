@extends('layouts.website.site')
<link rel="stylesheet" href="{{ asset('assets/Login/css/css-material-design-iconic-font.min.css') }}">

<!-- Main css -->
<link rel="stylesheet" href="{{ asset('assets/Login/css/css-style.css') }}">
@section('content')
    <!-- Sign up form -->
    <section class="signup">
        <div class="container bg-white shadow">
            <div class="signup-content">
                <div class="signup-form">
                    <h2 class="form-title">Sign up</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form method="POST" action="{{ route('website.post') }}" class="register-form" id="register-form">
                        @csrf
                        <div class="form-group">
                            <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                            <input type="text" name="name" id="name" placeholder="Your Name">
                        </div>
                        <div class="form-group">
                            <label for="userName"><i class="zmdi zmdi-account material-icons-name"></i></label>
                            <input type="text" name="user_name" id="user_name" placeholder="User Name">
                        </div>
                        <div class="form-group">
                            <label for="email"><i class="zmdi zmdi-email"></i></label>
                            <input type="email" name="email" id="email" placeholder="Your Email">
                        </div>
                        <div class="form-group">
                            <label for="email"><i class="zmdi zmdi-email"></i></label>
                            <input type="tel" name="phone" id="phone" placeholder="Your Phone">
                        </div>
                        <div class="form-group">
                            <label for=" "><i class="zmdi zmdi-google-earth"></i></label>
                            <input type="text" name="country" id="country" placeholder="Your Country">
                        </div>

                        <div class="form-group">
                            <label for=" "><i class="zmdi zmdi-google-earth"></i></label>
                            <input type="text" name="city" id="city" placeholder="Your City">
                        </div>
                        <div class="form-group">
                            <label for="text"><i class="zmdi zmdi-google-earth"></i></label>
                            <input type="text" name="state" id="state" placeholder="Your State">
                        </div>
                        <div class="form-group">
                            <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                            <input type="password" name="password" id="pass" placeholder="Password">
                        </div>

                        <div class="form-group">
                            <label for="role"><i class="zmdi zmdi-lock"></i></label>
                            <i class="zmdi zmdi-triangle-down angel"></i>
                            <select class="form-control" name="role"  id="role">
                                <option selected disabled hidden >Select Role..</option>
                                <option value="user">User</option>
                                <option value="organizer">Organizer</option>
                            </select>
                        </div>

                        <div class="form-group form-button">
                            <input type="submit" name="signup" id="signup" class="form-submit" value="Register">
                        </div>
                    </form>
                </div>
                <div class="signup-image" style="margin-top:100px!important;">
                    <figure><img src="assets/images/unnamed.png" alt="sing up image"></figure>
                    <a href="#" class="signup-image-link">Create an account</a>
                </div>
            </div>
        </div>
    </section>
    <script src="{{ asset('assets/Login/js/393-js-main.js') }}"></script>
@endsection
