@extends('layouts.website.site')
@section('content')
    <!-- Page Content -->
    <div class="page-heading page-title-bg header-text">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-content text-left">
                        <h2>All Sports Event</h2>
                        <h4>Get Tickets to enjoy the event</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="evnetlist">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="filters">
                        <ul>
                            <li class="active" data-filter="*">All </li>
                            <li data-filter=".ufc">UFC</li>
                            <li data-filter=".football">Football</li>
                            <li data-filter=".cricket">Cricket</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="filters-content">
                        <div class="row grid">
                            <div class="col-lg-4 col-md-4 all ufc">
                                <div class="product-item">
                                    <a href="#"><img src="assets/images/product_01.jpg" class="img-fluid" alt=""></a>
                                    <div class="down-content">
                                        <a href="#">
                                            <h4 class="text-red">Tittle goes here</h4>

                                        </a>
                                        <h5 class="text-muted f-14">21 May,2021</h5>
                                        <p>Lorem ipsume dolor sit amet, adipisicing elite. Itaque, corporis nulla aspernatur.</p>
                                        <ul class="star">
                                            <li>
                                                <a href="registerNow.html" class="filled-button">View Details</a>
                                            </li>
                                            <li>
                                                <a href="registerNow.html" class="filled-button">Buy Tickets</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4  all ufc">
                                <div class="product-item">
                                    <a href="#"><img src="assets/images/product_01.jpg" class="img-fluid" alt=""></a>
                                    <div class="down-content">
                                        <a href="#">
                                            <h4 class="text-red">Tittle goes here</h4>

                                        </a>
                                        <h5 class="text-muted f-14">21 May,2021</h5>
                                        <p>Lorem ipsume dolor sit amet, adipisicing elite. Itaque, corporis nulla aspernatur.</p>
                                        <ul class="star">
                                            <li>
                                                <a href="registerNow.html" class="filled-button">View Details</a>
                                            </li>
                                            <li>
                                                <a href="registerNow.html" class="filled-button">Buy Tickets</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4  all ufc">
                                <div class="product-item">
                                    <a href="#"><img src="assets/images/product_01.jpg" class="img-fluid" alt=""></a>
                                    <div class="down-content">
                                        <a href="#">
                                            <h4 class="text-red">Tittle goes here</h4>

                                        </a>
                                        <h5 class="text-muted f-14">21 May,2021</h5>
                                        <p>Lorem ipsume dolor sit amet, adipisicing elite. Itaque, corporis nulla aspernatur.</p>
                                        <ul class="star">
                                            <li>
                                                <a href="registerNow.html" class="filled-button">View Details</a>
                                            </li>
                                            <li>
                                                <a href="registerNow.html" class="filled-button">Buy Tickets</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4  all football">
                                <div class="product-item">
                                    <a href="#"><img src="assets/images/product_02.jpg" class="img-fluid" alt=""></a>
                                    <div class="down-content">
                                        <a href="#">
                                            <h4 class="text-red">Tittle goes here</h4>

                                        </a>
                                        <h5 class="text-muted f-14">21 May,2021</h5>
                                        <p>Lorem ipsume dolor sit amet, adipisicing elite. Itaque, corporis nulla aspernatur.</p>
                                        <ul class="star">
                                            <li>
                                                <a href="registerNow.html" class="filled-button">View Details</a>
                                            </li>
                                            <li>
                                                <a href="registerNow.html" class="filled-button">Buy Tickets</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4  all football">
                                <div class="product-item">
                                    <a href="#"><img src="assets/images/product_02.jpg" class="img-fluid" alt=""></a>
                                    <div class="down-content">
                                        <a href="#">
                                            <h4 class="text-red">Tittle goes here</h4>

                                        </a>
                                        <h5 class="text-muted f-14">21 May,2021</h5>
                                        <p>Lorem ipsume dolor sit amet, adipisicing elite. Itaque, corporis nulla aspernatur.</p>
                                        <ul class="star">
                                            <li>
                                                <a href="registerNow.html" class="filled-button">View Details</a>
                                            </li>
                                            <li>
                                                <a href="registerNow.html" class="filled-button">Buy Tickets</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4  all football">
                                <div class="product-item">
                                    <a href="#"><img src="assets/images/product_02.jpg" class="img-fluid" alt=""></a>
                                    <div class="down-content">
                                        <a href="#">
                                            <h4 class="text-red">Tittle goes here</h4>

                                        </a>
                                        <h5 class="text-muted f-14">21 May,2021</h5>
                                        <p>Lorem ipsume dolor sit amet, adipisicing elite. Itaque, corporis nulla aspernatur.</p>
                                        <ul class="star">
                                            <li>
                                                <a href="registerNow.html" class="filled-button">View Details</a>
                                            </li>
                                            <li>
                                                <a href="registerNow.html" class="filled-button">Buy Tickets</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4  all cricket">
                                <div class="product-item">
                                    <a href="#"><img src="assets/images/product_03.jpg" class="img-fluid" alt=""></a>
                                    <div class="down-content">
                                        <a href="#">
                                            <h4 class="text-red">Tittle goes here</h4>

                                        </a>
                                        <h5 class="text-muted f-14">21 May,2021</h5>
                                        <p>Lorem ipsume dolor sit amet, adipisicing elite. Itaque, corporis nulla aspernatur.</p>
                                        <ul class="star">
                                            <li>
                                                <a href="registerNow.html" class="filled-button">View Details</a>
                                            </li>
                                            <li>
                                                <a href="registerNow.html" class="filled-button">Buy Tickets</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4  all cricket">
                                <div class="product-item">
                                    <a href="#"><img src="assets/images/product_03.jpg" class="img-fluid" alt=""></a>
                                    <div class="down-content">
                                        <a href="#">
                                            <h4 class="text-red">Tittle goes here</h4>

                                        </a>
                                        <h5 class="text-muted f-14">21 May,2021</h5>
                                        <p>Lorem ipsume dolor sit amet, adipisicing elite. Itaque, corporis nulla aspernatur.</p>
                                        <ul class="star">
                                            <li>
                                                <a href="registerNow.html" class="filled-button">View Details</a>
                                            </li>
                                            <li>
                                                <a href="registerNow.html" class="filled-button">Buy Tickets</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4  all cricket">
                                <div class="product-item">
                                    <a href="#"><img src="assets/images/product_03.jpg" class="img-fluid" alt=""></a>
                                    <div class="down-content">
                                        <a href="#">
                                            <h4 class="text-red">Tittle goes here</h4>

                                        </a>
                                        <h5 class="text-muted f-14">21 May,2021</h5>
                                        <p>Lorem ipsume dolor sit amet, adipisicing elite. Itaque, corporis nulla aspernatur.</p>
                                        <ul class="star">
                                            <li>
                                                <a href="registerNow.html" class="filled-button">View Details</a>
                                            </li>
                                            <li>
                                                <a href="registerNow.html" class="filled-button">Buy Tickets</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <ul class="pages">
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection