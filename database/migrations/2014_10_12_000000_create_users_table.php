<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\User;
use Spatie\Permission\Models\Role;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('user_name');
            $table->string('phone');
            $table->string('country');
            $table->string('city');
            $table->string('state');
            $table->rememberToken();
            $table->timestamps();
        });


        $user = new User;
        $user->name = 'admin';
        $user->email  = 'admin@test.com';
        $user->password = bcrypt('12345678');
        $user->user_name = 'admin';
        $user->phone = '-';
        $user->country = '-';
        $user->city = '-';
        $user->state = '-';
        $user->save();


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
