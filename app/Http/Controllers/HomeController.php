<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\User;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $totalUsers = User::count();
        $totalEvent = Event::count();
        $latestEvent = Event::latest()->get()->first();

        $cruuentEvent=Event::whereDate('date', Carbon::today())->orderBy('created_at','asc')->first();


        return view('home', compact('totalUsers', 'totalEvent','cruuentEvent','latestEvent'));
    }
}
