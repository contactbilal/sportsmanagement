<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\FileUploadService;
use App\EventHaveTeam;
use App\Categories;
use App\Event;
use App\Team;
use Auth;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::latest()->get();
        return view('admin.event.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categories::latest()->get();
        return view('admin.event.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // 'avtar '  => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|required',

     $validateData = $request->validate([
        'event_name' => 'required',
        'time' => 'required',
        'location' => 'required',
        'description' => 'required',
        'category_id' => 'required',
     ]);

    //  $file = FileUploadService::upload( $request->file('avtar'), 'public/profile/' );

     $event = new Event;
     $event->organizer_id = Auth::user()->id;
     $event->category_id = $request->category_id;
     $event->event_name = $request->event_name;
     $event->date  = $request->date;
     $event->avtar ='dumy';
     $event->time = $request->time;
     $event->location = $request->location;
     $event->description = $request->description;

     $event->save();


     $request->session()->flash('msg','Record successfully inserted');
     return redirect()->route('events.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event=Event::find($id);
        return view('admin.event.show',compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::find($id);
        $categories = Categories::latest()->get();

        return view('admin.event.edit',['event'=>$event,'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'event_name' => 'required',
            'time' => 'required',
            'location' => 'required',
            'description' => 'required',
            'category_id' => 'required',
         ]);



         $event =Event::find($id);
         $event->organizer_id = Auth::user()->id;
         $event->category_id = $request->category_id;
         $event->event_name = $request->event_name;
         $event->date  = $request->date;
         $event->avtar ='dumy';
         $event->time = $request->time;
         $event->location = $request->location;
         $event->description = $request->description;

         $event->save();


         $request->session()->flash('msg','Record successfully inserted');
         return redirect()->route('events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $data = EventHaveTeam::where('event_id', $id)->get();
        if($data !== null){
            foreach ($data as $key => $event) {
                # code...
                $d = EventHaveTeam::where('id', $event->id)->first();
                $d->delete();
            }
        }

        $event=Event::find($id);
        $event->delete();
        if ($event != null){
            $event->delete();
            session()->flash('msg','User Deleted Successfully');
        }

        return back();
    }

    public function showTeams($event_id){


        $event = Event::find($event_id);
        $getExistingTeams = EventHaveTeam::where('event_id', $event_id)->get()->pluck('team_id')->toArray();
        $teams = Team::where('organizer_id', Auth::user()->id)->whereNotIn('id', $getExistingTeams)->latest()->get();
        return view('admin.event.selectTeam',compact('event', 'teams'));

    }

    public function addTeams(Request $request){

        $validateData = $request->validate([
            'event_id' => 'required',
            'team' => 'required',
         ]);

         foreach ($_POST['team'] as $key => $value) {
            # code...
            $teamplayer = new EventHaveTeam;
            $teamplayer->event_id = $request->event_id;
            $teamplayer->team_id = $value;
            $teamplayer->save();

        }

        return back();

    }

}
