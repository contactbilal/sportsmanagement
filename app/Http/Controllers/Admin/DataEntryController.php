<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\User;

class DataEntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataEntry=User::role('data-entry')->get();
        return view('admin.dataEntry.index',compact('dataEntry'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.dataEntry.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'user_name' => 'required|unique:users',
            'phone' => 'required',
            'country' => 'required',
            'city' => 'required',
            'state' => 'required',
          ]);

        $user = new User;
        $user->name = $request->name;
        $user->email  = $request->email ;
        $user->password = bcrypt($request->password);
        $user->user_name = $request->user_name;
        $user->phone = $request->phone;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->save();

        $user->assignRole('data-entry');
        $request->session()->flash('msg','Record successfully inserted');
        return redirect()->route('data-entry.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataEntry=User::find($id);
        return view('admin.dataEntry.show',compact('dataEntry'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataEntry=User::find($id);
        return view('admin.dataEntry.edit',['dataEntry'=>$dataEntry]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([

            'email' => ['required',
                Rule::unique('users')->ignore($id)],
            'user_name' => ['required',
                Rule::unique('users')->ignore($id)],
            'name' => 'required',
            'phone' => 'required',
            'country' => 'required',
            'city' => 'required',
            'state' => 'required',
          ]);

        $user=  User::find($id);

        $user->name = $request->name;
        $user->email  = $request->email ;
        $user->user_name = $request->user_name;
        $user->phone = $request->phone;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->save();


        // message
        $request->session()->flash('msg','Record Updated');
        // redirect to home
        return redirect()->route('data-entry.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataEntry=User::find($id);
        $dataEntry->roles()->detach();
        $dataEntry->delete();
        if ($dataEntry != null){
            $dataEntry->delete();
            session()->flash('msg','User Deleted Successfully');
        }

        return back();
    }
}
