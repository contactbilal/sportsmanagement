<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\FileUploadService;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\TeamHavePlayer;
use App\Team;
use App\Player;
use Auth;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams=Team::latest()->get();
        return  view('admin.team.index',compact('teams'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'team_name' => 'required',
            'email' => 'required|email|unique:player',
            'avtar'  => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|required',
            'phone' => 'required',
            'country' => 'required',
            'city' => 'required',
            'state' => 'required',
         ]);

         $file = FileUploadService::upload( $request->file('avtar'), 'public/profile/' );

         $team = new Team;
         $team->organizer_id = Auth::user()->id;
         $team->team_name = $request->team_name;
         $team->email  = $request->email;
         $team->avtar =$file;
         $team->phone = $request->phone;
         $team->country = $request->country;
         $team->city = $request->city;
         $team->state = $request->state;

         $team->save();


         $request->session()->flash('msg','Record successfully inserted');
         return redirect()->route('teams.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team=Team::find($id);
        return view('admin.team.edit',['team'=>$team]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'team_name' => 'required',
            'email' => ['required',
            Rule::unique('player')->ignore($id)],
            'phone' => 'required',
            'country' => 'required',
            'city' => 'required',
            'state' => 'required',
         ]);


         $team =Team::find($id);
         $team->team_name = $request->team_name;
         $team->email  = $request->email;
         $team->phone = $request->phone;
         $team->country = $request->country;
         $team->city = $request->city;
         $team->state = $request->state;
         $team->save();
         return  redirect()->route('teams.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = TeamHavePlayer::where('team_id', $id)->get();
        if($data !== null){
            foreach ($data as $key => $team) {
                # code...
                $d = TeamHavePlayer::where('id', $team->id)->first();
                $d->delete();
            }
        }
        $team=Team::find($id);
        $team->delete();
        if ($team != null){
            $team->delete();
            session()->flash('msg','User Deleted Successfully');
        }

        return back();
    }

    public function showPlayers($team_id){

        $team = Team::find($team_id);
        $getExistingPlayers = TeamHavePlayer::where('team_id', $team_id)->get()->pluck('player_id')->toArray();
        $players = Player::where('organizer_id', Auth::user()->id)->whereNotIn('id', $getExistingPlayers)->latest()->get();
        return view('admin.team.selectPlayer',compact('team', 'players'));

    }

    public function addPlayers(Request $request){

        $validateData = $request->validate([
            'team_id' => 'required',
            'player' => 'required',
         ]);


        foreach ($_POST['player'] as $key => $value) {
            # code...
            $teamplayer = new TeamHavePlayer;
            $teamplayer->team_id = $request->team_id;
            $teamplayer->player_id = $value;
            $teamplayer->save();
        }

        return back();

    }
}
