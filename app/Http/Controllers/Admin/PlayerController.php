<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\FileUploadService;
use App\Player;
use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Auth;

class PlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $players=Player::latest()->get();
        return view('admin.player.index',compact('players'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.player.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



     $validateData = $request->validate([
        'player_name' => 'required',
        'email' => 'required|email|unique:player',
        'avtar'  => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|required',
        'phone' => 'required',
        'country' => 'required',
        'city' => 'required',
        'state' => 'required',
     ]);

     $file = FileUploadService::upload( $request->file('avtar'), 'public/profile/' );

     $player = new player;
     $player->organizer_id = Auth::user()->id;
     $player->player_name = $request->player_name;
     $player->email  = $request->email;
     $player->avtar =$file;
     $player->phone = $request->phone;
     $player->country = $request->country;
     $player->city = $request->city;
     $player->state = $request->state;
     $player->enrolled = 0;
     $player->save();


     $request->session()->flash('msg','Record successfully inserted');
     return redirect()->route('palyers.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $player=Player::find($id);
        return view('admin.player.showPlayer',compact('player'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $player = Player::find($id);
        return view('admin.player.edit',['player'=>$player]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'player_name' => 'required',
            'email' => ['required',
            Rule::unique('player')->ignore($id)],
            'phone' => 'required',
            'country' => 'required',
            'city' => 'required',
            'state' => 'required',
         ]);


         $player =Player::find($id);
         $player->player_name = $request->player_name;
         $player->email  = $request->email;
         $player->phone = $request->phone;
         $player->country = $request->country;
         $player->city = $request->city;
         $player->state = $request->state;
         $player->save();
         return  redirect()->route('palyers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $player=Player::find($id);
        $player->delete();
        if ($player != null){
            $player->delete();
            session()->flash('msg','User Deleted Successfully');
        }

        return back();
    }
}
