<?php

namespace App\Http\Controllers\Website;
use App\Models\ContentPage;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Event;
use App\Categories;
use App\News;

use Carbon\Carbon;


class PageController extends Controller
{
    public function __invoke($slug){
        // event details
        $totalEvent = Event::count();
        $currentEvent=Event::whereDate('date', Carbon::today())->orderBy('created_at','asc')->count();
        $totalUpcomingEvents=Event::whereDate('date', Carbon::now()->add(2, 'day'))->count();

        //Latest (all,6) Record of event
        $allEventsShow=News::latest()->get();
        $latestEvents=Event::latest()->take(6)->get();

        //Latest (all,6) Record of event
        $allNewsShow=News::latest()->get();
        $latestNews=News::latest()->take(6)->get();

        // latest categories
        $allCategories = Categories::latest()->get();

// dd( $allCategories);
        if($slug ==  'home'){
            return view('website.pages.home',compact('totalEvent','currentEvent','totalUpcomingEvents','latestEvents','allCategories','latestNews'));
        } elseif($slug == 'about'){
            return view('website.pages.about');
        }
        elseif($slug == 'eventList'){
            return view('website.pages.eventList',compact('allEventsShow','totalEvent','currentEvent','totalUpcomingEvents'));
        }
        elseif($slug == 'news'){
            return view('website.pages.news',compact('allNewsShow','totalEvent','currentEvent','totalUpcomingEvents'));
        }
        elseif($slug == 'schedule'){
            return view('website.pages.schedule');
        }
        elseif($slug == 'contact'){
            return view('website.pages.contact');
        }
        elseif($slug == 'login'){
            return view('website.pages.login');
        }
        elseif($slug == 'signup'){
            return view('website.pages.signup');
        }

    }

    public static function eventCategoryPage($id){

        $event=Event::find($id);
        $categoryId=$event->category_id;

        $categories = Categories::find($categoryId);

        if($categories->slug=="ufc"){
            return view('website.eventTemplates.ufc');
        }else if($categories->slug=="cricket"){
            return view('website.eventTemplates.cricket');
        }elseif($categories->slug=="football"){
            return view('website.eventTemplates.football');
        }
    }


    public static function showNews($id){

        $news=News::find($id);
        return view('website.pages.newsdetail',compact('news'));

    }
}
