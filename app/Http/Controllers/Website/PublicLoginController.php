<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
class PublicLoginController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = new User;

        $user->email  = $request->email ;
        $user->password = bcrypt($request->password);
        $user->assignRole($request->role);
        $credentials = $request->only('email' , 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...

            if(Auth::user()->hasRole('user')){

                // return redirect()->route('userdashboard');
                return redirect()->route('userdashboard.home');

              }elseif(Auth::user()->hasRole('organizer')){

                return 'I am Organizer';

              }


        }
    }
}
