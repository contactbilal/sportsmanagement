<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{

    protected $table = 'player';

    protected $fillable = [
        'organizer_id','avtar','player_name','email','phone','country','city','state','enrolled'
    ];

}
