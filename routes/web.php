<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function (){
    return Redirect::to('/home');
});

Route::prefix('dashboard')->group(function () {
    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');
});
Route::namespace('Admin')
->prefix('dashboard')
->middleware('auth')
->group(function () {
    Route::resource('organizers', OrganizerController::class);
    Route::resource('data-entry', DataEntryController::class);
    Route::resource('users', UserController::class);
    Route::resource('palyers', PlayerController::class);
    Route::resource('category', CategoryController::class);
    Route::resource('news', NewsController::class);

    Route::resource('events', EventController::class);
    Route::get('show_teams/{event_id}', 'EventController@showTeams')->name('event.show.team');
    Route::post('add_teams', 'EventController@addTeams')->name('event.add.team');

    Route::resource('teams', TeamController::class);
    Route::get('show_players/{team_id}', 'TeamController@showPlayers')->name('team.show.player');
    Route::post('add_players', 'TeamController@addPlayers')->name('team.add.player');

});

Route::namespace('Website')
->group(function () {

    Route::get('{slug}', PageController::class);
    Route::get('show_event/{id}', 'PageController@eventCategoryPage')->name('event.category.page');
    Route::get('show_news/{id}', 'PageController@showNews')->name('show.news.detail.page');

    Route::post('/new/register', PublicRegisterController::class)->name('website.post');
    Route::post('/new/Login', PublicLoginController::class)->name('website.login');

});

// clientdashboard and organizer
// Route::group(['middleware' => 'auth'],function () {
//     Route::get('/userdashboard', function () {
//         return view('website.clientDashboard.index');
//     })->name('userdashboard');

// });
Route::namespace('ClientDashboard')
 ->middleware(['auth'])
 ->as('userdashboard.')
 ->prefix('userdashboard')
 ->group(function(){
     Route::get('/home', HomeController::class)->name('home');
 });

